﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace randomPhoto
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            StringBuilder html = new StringBuilder("<html><body bgcolor=black><table width=100%><tr>");
            Dictionary<DirectoryInfo, FileInfo[]> tree = new Dictionary<DirectoryInfo, FileInfo[]>();
            DirectoryInfo[] folders = new DirectoryInfo(args.Length > 0 ? args[0] : ".").GetDirectories();
            try
            {
                int width_pic_px = 1280 / folders.Length;
                foreach (DirectoryInfo dir in folders)
                {
                    Console.WriteLine(dir.Name);
                    FileInfo[] files = dir.GetFiles("*.*", SearchOption.AllDirectories);
                    if (files.Length == 0)
                    {
                        html.Append("<td><center><font color=white>" + dir.Name + "<br>пуста</td>");
                        continue;
                    }
                    tree.Add(dir, files);
                    int random_pic = rnd.Next(tree[dir].Length);

                    html.Append(string.Format("<td><center><font size=2 color=white>{0}<br><a href=\"{2}\"><img width={1}px src=\"{2}\"></a><br>{3}</td>",
                       dir.Name,
                       width_pic_px,
                       tree[dir][random_pic].FullName,
                       tree[dir][random_pic].Name));
                }

                string out_filename = string.Format("randomPhoto {0}.html", DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss"));
                File.WriteAllText(out_filename, html.ToString());
                Process.Start(out_filename);
            }
            catch (DivideByZeroException ex)
            {
                Console.WriteLine("Нет папок?");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
